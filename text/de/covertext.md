Ich fühle mich bedrückt, unleidig, vom Leben ungerecht behandelt, habe Kopfschmerzen, will am liebsten morgens im Bett bleiben, schaue mit Argwohn auf Menschen um mich herum, werde unfair ... Es geht mir nicht so toll.
Dies alles sind Symptome des Irrgefühls.
Er ist eigentlich kein Gefühl sondern ein Zustand, aber wir fühlen uns dabei ... hmmm ... nicht toll.

Iris hat über 60 Jahre ihres Lebens in ihrer ureigenen autistischen Akribie, damit verbracht, Phänomene wie Irrgefühl (im Schwedischen Ångest) zu erforschen, zu verstehen und auf eine Weise zu beschreiben, dass Menschen frei sind, ihre Gedanken zu nehmen oder zu lassen. Meist in Gesprächsrunden und Einzelgesprächen.

Ein solches Gespräch ist die Grundlage dieses Buches.
Was Iris sagt, ist oft unscheinbar, man kann daran vorbeigehen, etwas nehmen, etwas liegen lassen, wie man sich fühlt.
Aber wenn man eintaucht ...
